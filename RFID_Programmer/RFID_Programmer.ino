#include <SPI.h>
#include <MFRC522.h>

//Pins while using PU04 as programmer
#define RST_PIN      2
#define SS_PIN      0

//Pins while using Arduino as programmer
//#define RST_PIN      9
//#define SS_PIN      10

#define NUM_SCENTS  144

//define for programming jerrycans else it will program cartridges
//#define JERRYCAN


const char scents[] PROGMEM = {
"Algues & Seawater\n\
Aloe Vera\n\
Anijs & Eucalyptus (etherisch)\n\
Anijs (etherisch)\n\
Anti-Smoke\n\
Aqua di Parma\n\
Baby Soft\n\
Bamboo\n\
Bergamot & Sinaasappel (etherisch)\n\
Black Dahlia\n\
Blue Wave\n\
Breathing (etherisch)\n\
Cafe Latte\n\
Cappucino (Special)\n\
Citroen & Sinaasappel\n\
Citronelle & Kaffir\n\
Dennen & Eucalyptus (etherisch)\n\
Dennen (etherisch)\n\
Easy Breeze (etherisch)\n\
Eclats de Bergamote\n\
Eco Blue\n\
Eco Brown\n\
Eco Green\n\
Eucalyptus (etherisch)\n\
Fizzy Lemon\n\
Fresh Coffee Beans (Special)\n\
Frisse Regenwoud\n\
Funny Valentine\n\
Giornissimo\n\
Green Apple\n\
Honey Flower\n\
Honing & Meloen\n\
Hot Chocolate (Special)\n\
Jasmin Secret\n\
Karakter\n\
Kerst\n\
Lady Luxe\n\
Lavendel & Dennen (etherisch)\n\
Lavendel & Eucalyptus (etherisch)\n\
Lavendel & Sinaasappel\n\
Lavendel (etherisch)\n\
Lavendel Supreme\n\
Lime Tree\n\
Linnenfris\n\
Luxe\n\
Maison\n\
Marakesh Market\n\
Melissa & Eucalyptus (etherisch)\n\
Mintsport\n\
Oriëntal\n\
Oudh Hammam\n\
Peer\n\
Peppermint (etherisch)\n\
Pink Pepper Amberwood\n\
Rozemarijn & Dennen\n\
Rozemarijn & Eucalyptus (etherisch)\n\
R_Black Oud\n\
R_Fresh Spice\n\
R_Hammam\n\
R_Marquises\n\
R_Red Currant\n\
Sandelwood\n\
She's Fresh\n\
Sinaasappel & Ceder\n\
Sinaasappel (etherisch)\n\
Smoke (Specials)\n\
Spice Clove (etherisch)\n\
Spiritual Flower\n\
Sweet Apple Pie\n\
Tranquility\n\
Tuscany Ridge (etherisch)\n\
Vers Gemaaid Gras\n\
Wilde Vijg\n\
Woodborough\n\
Woodborough/Lime Tree\n\
Xourthe\n\
Z _Bourbon\n\
Z_Budget_Lavender\n\
Z_Budget_Peppermint\n\
Z_Comfort_AntiSmoke\n\
Z_Comfort_Bakhour\n\
Z_Comfort_Fougere\n\
Z_Eucalyptus\n\
Z_Exclusive_Gentleman's_Agreement\n\
Z_Laughing Buddha\n\
Z_Rich\n\
Z_Sweetblossom\n\
Z_TheTaoRitual\n\
Z_UnderAFigTree\n\
Tough Woods\n\
Bos\n\
Scent Sensorik\n\
Kokos en Ananas\n\
Eclats de Bergamote\n\
Citronelle & Kaffir\n\
100Restrooms\n\
Pure Luxe\n\
Cookies\n\
Zhourba\n\
Sinaasappel niet Etherisch\n\
Rituals Ritual of Dao\n\
Rituals Ritual of Happy Buddha\n\
Rituals Ritual of Hammam\n\
Cavallaro\n\
Green Cardamom\n\
Black Oudh\n\
Lava\n\
Bourbon\n\
Fugi Mix\n\
100Restrooms vernevelolie\n\
Oolaboo\n\
Kokos\n\
Cederwood\n\
Cappucinno\n\
Care Sense\n\
Lavendel & Pepermunt\n\
Green Tea\n\
Spain\n\
Tough Woods\n\
Rituals Ritual of Karma\n\
Spring Flower  \n\
Medical\n\
Dottel\n\
Revitalizing\n\
Fresh cut grass\n\
Bubblelicious\n\
Grodenta Relaxing\n\
Grodenta Calming\n\
Grodenta Fearless\n\
Spring Story\n\
Summer Story\n\
Autumn Story\n\
Winter Story\n\
Lemon\n\
Walk in the woods\n\
Karamel\n\
Nyasia's Nature\n\
Coccinelle\n\
Rebellion\n\
Eucalyptus en Pepermunt\n\
Mystic woods\n\
Lemon en vanille\n\
Rapeseed Flower\n\
Anis / Peppermint\n"
};

char scentBuffer[40];

void getScent(int i, char* output, int len) {
  if (i==0) {
    strcpy(output, "No scent");
    return;
  } else if (i>NUM_SCENTS) {
    strcpy(output, "Unkown");
    return;
  }
  i--;
  int index = 0;
  const char* scent_p = scents;
  char c;
  while(index < i && index < NUM_SCENTS) {
    c = pgm_read_byte_near(scent_p);
    if (c == '\n') {
      index++;
    }
    scent_p++;
  }
  index = 0;
  while(index < len) {
    c = pgm_read_byte_near(scent_p + index);
    if (c== '\n') break;
    else output[index] = c;
    index++;
  }
  output[index] = 0;
}

void showScentList() {
  Serial.println("\nScent list:");
  for (int i=0; i<=NUM_SCENTS; i++) {
    getScent(i, scentBuffer, 40);
    Serial.print("#");
    Serial.print(i);
    Serial.print(": ");
    Serial.println(scentBuffer);
  }
  Serial.println("?: List scents");
  #ifdef JERRYCAN
  Serial.println("-----> JERRYCAN - You are now programming a JERRYCAN + NEW NOZZLE<-----\n");
  #else
  Serial.println("-----> CARTRIDGE - You are now programming a CARTRIDGE + NEW NOZZLE<-----\n");
  #endif
}

MFRC522 mfrc522(SS_PIN, RST_PIN); // Create MFRC522 instance

unsigned long readPage(int pageNum) {
  byte buffer[18];
  byte size = sizeof(buffer);
  
  MFRC522::StatusCode status = mfrc522.MIFARE_Read(pageNum, buffer, &size);
  
  unsigned long currentValue = 0;
  for(int i=0; i<4; i++) {
    currentValue += buffer[i] << ((3-i) * 8); //data behind 0x;
  }
  
  return currentValue;
}

String checkCartridgeVersion(){
  String type = "0";
  

    mfrc522.PICC_IsNewCardPresent();

    if (!mfrc522.PICC_IsNewCardPresent())
      return type;
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return type;
  }
  
  //mfrc522.PCD_NTAG216_AUTH(&PSWBuff[0], pACK); //Request Authentification if return STATUS_OK we are good
  byte byteCount;
  byte buffer[18];
  // Read scent ID
  byteCount = sizeof(buffer);
  mfrc522.MIFARE_Read(8, buffer, &byteCount);
  
  for (byte index = 0; index < 4; index++){
    type += String(buffer[index], HEX); //data behind 0x;
  } 
  return type;
}


unsigned long readNumber = 0, programNumber = 0;
bool gotNumber = false;
byte WBuff[] = {0x01, 0x02, 0x03, 0x04};

void setup() {
  Serial.begin(115200);   // Initialize serial communications with the PC
  while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
  SPI.begin();      // Init SPI bus
  mfrc522.PCD_Init();   // Init MFRC522
  showScentList();
}

void loop() {
  if (Serial.available()) { 
    char c = Serial.read();
    if (c >= '0' && c <= '9') {
      gotNumber = true;
      readNumber *= 10;
      readNumber += c - '0';
    } else if ((c=='\r' || c=='n') && gotNumber) {
      gotNumber = false;
      
      if (readNumber == 0) {
        Serial.println("I will stop programming and start reading scents");
      } else {
        Serial.print("I will now start programming scent #");
        Serial.print(readNumber);
        Serial.print(": ");
        getScent(readNumber, scentBuffer, 40);
        Serial.println(scentBuffer);
      }
      
      programNumber = readNumber;
      readNumber = 0;

      for (int i=0; i<4; i++) {
        WBuff[i] = (programNumber >> ((3-i)*8)) & 0xFF;
      }
    } else if(c == '?') {
      showScentList();
    }
  }
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    //Serial.println("No new Card Found");
    return;
  }

  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    //Serial.println("Can't read UID");
    return;
  }
  
  
  byte Nop[] = {0,0,0,0}; //empty memory page

  #ifdef JERRYCAN
  byte type[] = {0x6a,0x65,0x72,0x00};//jerrycan
  #else
  byte type[] = {0x63,0x61,0x72,0x00};//cartridge
  #endif
  
  //byte type[] = {0x63,0x61,0x72,0x00};//cartridge
  //byte type[] = {0x6a,0x65,0x72,0x00};//jerrycan

    //cartridge fill values
  //byte huts[] = {0x00,0x0F,0x42,0x40}; //fill = 1.000.000
  //byte huts[] = {0x00,0x2D,0xC6,0xC0}; //fill = 3.000.000
  //byte huts[] = {0x00,0x5B,0x8D,0x80}; //fill = 6.000.000

  //jerrycan fill values
  //byte huts[] = {0x00,0x5B,0x8D,0x80}; //fill = 6.000.000
  //byte huts[] = {0x01,0x2D,0xB1,0x80}; //fill = 22.000.000
  //byte huts[] = {0x01,0xC9,0xC3,0x80}; //fill = 30.000.000

  byte nozzleVersion[] = {0x00,0x00,0x00,0x01};//nozzleversion
  byte fillTest[] = {0x00,0x00,0x75,0x30}; //fill = ?
 
  if (programNumber != 0) { //write tag
    mfrc522.MIFARE_Ultralight_Write(4,WBuff, 4);  //Write scent
    mfrc522.MIFARE_Ultralight_Write(6,fillTest, 4); //Reset useage
    mfrc522.MIFARE_Ultralight_Write(7,Nop, 4); //Reset first use
    mfrc522.MIFARE_Ultralight_Write(8,type, 4); //Reset first use
    mfrc522.MIFARE_Ultralight_Write(9,nozzleVersion, 4); //Reset nozzle version
    //mfrc522.MIFARE_Ultralight_Write(10,fillTest, 4); //Reset useage
    unsigned long scentRead = readPage(4); //read scent
    
    getScent(scentRead, scentBuffer, 40);
    
    Serial.print("Scent is now: #");
    Serial.print(scentRead);
    Serial.print(": ");
    Serial.println(scentBuffer);
  } else { //read tag
    unsigned long scentRead = readPage(4); //read scent
    //unsigned long useageRead = readPage(6); //read useage
    unsigned long firstUseRead = readPage(7); //read first use
    String cartridgeTypeRead = checkCartridgeVersion();
    unsigned long nozzleVersionRead = readPage(9); //read the nozzle version
    unsigned long testFill = readPage(6); //read the nozzle version
    
    getScent(scentRead, scentBuffer, 40);
    
    Serial.print("Scent: #");
    Serial.print(scentRead);
    Serial.print(": ");
    Serial.println(scentBuffer);
    Serial.print("Useage: ");
    Serial.println(testFill);
    Serial.print("First use: ");
    Serial.println(firstUseRead);

    //Is it a cartridge or a jerrycan?
    if (cartridgeTypeRead == "06361720"){
      Serial.print("Cartridge Type: CARTRIDGE\n"); 
    }
    else if (cartridgeTypeRead == "06a65720"){
      Serial.print("Cartridge Type: JERRYCAN\n");
    }
    else{
      Serial.print("Cartridge Type: NO CARTRIDGE TYPE DETECTED\n");
    }

    Serial.print("Nozzle version: ");
    Serial.println(nozzleVersionRead);
  }
  Serial.println();
  
  delay(500);
}
